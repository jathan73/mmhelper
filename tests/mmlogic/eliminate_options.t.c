#include <stdlib.h>
#include <assert.h>
#include <stdio.h>

#include "mmlogic.h"

int main() {

    {
        enum code_selection option_set[8][8][8][8][8] = {POSSIBLE};
        enum code_selection resulting_option_set[8][8][8][8][8] = {POSSIBLE};
        unsigned int guess[5] = {1, 1, 1, 1, 0};
        unsigned int result = 10;
        eliminate_options(option_set, 4, guess, result, resulting_option_set);
        if (resulting_option_set[1][2][2][2][0] != POSSIBLE) {
            printf("FAIL: eliminate_options({1,2,2,2,0}, 4, 10) should have NOT eliminated {2,2,2,2,0}, but did!\n");
            return EXIT_FAILURE;
        }
    }

    {
        enum code_selection option_set[8][8][8][8][8] = {POSSIBLE};
        enum code_selection resulting_option_set[8][8][8][8][8] = {POSSIBLE};
        unsigned int guess[5] = {1, 2, 3, 4, 0};
        unsigned int result = 10;
        eliminate_options(option_set, 4, guess, result, resulting_option_set);
        if (resulting_option_set[2][1][2][2][0] != DISCARDED) {
            printf("FAIL: eliminate_options({1,2,3,4}, 4, 10) should have eliminated {2,1,2,2}, but didn't!\n");
            return EXIT_FAILURE;
        }
    }

    {
        enum code_selection option_set[8][8][8][8][8] = {POSSIBLE};
        enum code_selection resulting_option_set[8][8][8][8][8] = {POSSIBLE};
        unsigned int guess[5] = {1, 1, 1, 1, 0};
        unsigned int result = 10;
        eliminate_options(option_set, 5, guess, result, resulting_option_set);
        if (resulting_option_set[2][2][2][2][0] != POSSIBLE) {
            printf("FAIL: eliminate_options({1,1,1,1,0}, 5, 10) should have NOT eliminated {2,2,2,2,0}, but did!\n");
            return EXIT_FAILURE;
        }
    }

    {
        enum code_selection option_set[8][8][8][8][8] = {POSSIBLE};
        enum code_selection resulting_option_set[8][8][8][8][8] = {POSSIBLE};
        unsigned int guess[5] = {1, 2, 3, 4, 0};
        unsigned int result = 20;
        eliminate_options(option_set, 5, guess, result, resulting_option_set);
        if (resulting_option_set[1][2][3][3][0] != DISCARDED) {
            printf("FAIL: eliminate_options({1,2,3,4,0}, 5, 20) should have eliminated {1,2,3,3,0}, but didn't!\n");
            return EXIT_FAILURE;
        }
    }
    
    return EXIT_SUCCESS;
}
